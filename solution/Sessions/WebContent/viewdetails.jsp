<%@ page import="java.util.*" %>

<%
	String fullname = "";
	String username = (String) session.getAttribute("username");
	String age = "";
	String email = "";
	String[] interests = {};
	Cookie[] cookies = request.getCookies();
	for (int i = 0; i < cookies.length; i++) {
		if (cookies[i].getName().equals("fullname")) {
			fullname = cookies[i].getValue();
		}
		if (cookies[i].getName().equals("age")) {
			age = cookies[i].getValue();
		}
		if (cookies[i].getName().equals("email")) {
			email = cookies[i].getValue();
		}
		if (cookies[i].getName().equals("interests")) {
			String interestString = cookies[i].getValue();
			interests = getBits(interestString);
		}
	}
%>


<DIV align="left">
<H2>Your details: </H2>

<TABLE border="0">
<TR>
<TD width="100" align="right"><B>Name:</B></TD>
<TD align="left"><%= fullname %></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Username:</B></TD>
<TD align="left"><%= username %></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Age:</B></TD>
<TD align="left"><%= age %></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Email:</B></TD>
<TD align="left"><%= email %></TD>
</TR>
<TR>
<TD width="100" align="right" valign="top"><B>Interests:</B></TD>
<TD align="left">
<% if (interests.length > 0) { %>
	<UL>
	<% for (int i = 0; i < interests.length; i++) { %>
		<LI><%= interests[i] %></LI>
	<% } %>
	</UL>
<% } else { %>
	<I>none</I><% } 
%>
</TD>
</TR>
</TABLE>
</DIV>


<%!
	private String[] getBits(String s) {
		StringTokenizer st = new StringTokenizer(s, " ");
		ArrayList res = new ArrayList();
		while (st.hasMoreTokens()) {
			res.add(st.nextToken());
		}
		return (String[]) res.toArray(new String[0]);	
	}
%>
