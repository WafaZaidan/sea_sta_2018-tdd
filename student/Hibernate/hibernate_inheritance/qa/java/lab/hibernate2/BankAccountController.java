package qa.java.lab.hibernate2;
/* Here we can explore aspects of ORM with Hibernate
 * 1. Add inheritance to the bank account model and read details from database.
 * 2. Implement polymorphic rule-checking in the withdraw operation.
 * 
 * 
 *  Remember that there are scripts in the "labs" directory to re-create and re-populate the
 *  database, so that you can refresh the data at any time by running the scripts inside Toad
 *  (or from a command line via the mysql command if you really want to!).
 *  
 *      */

import java.util.*;

import javax.swing.JOptionPane;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.*;
import javax.swing.*;

public class BankAccountController {

	//the persistence manager object:
	private Session session;

	//initialise Hibernate and obtain the Session object
	public void init() {
		Configuration config = null;
		config = new Configuration();
		config.configure();
		
		ServiceRegistry sr = 
				new ServiceRegistryBuilder().
				applySettings(config.getProperties()).
				buildServiceRegistry();

		//define the mapping classes
		config.addAnnotatedClass(BankAccount.class);
		config.addAnnotatedClass(BankTransaction.class);
		
		SessionFactory sf = config.buildSessionFactory(sr);

		//use the SessionFactor to get a Session (store in instance var)
		session = sf.openSession();

	}
	
	public void close(){
		session.close();	
	}
	
	public void listAccounts(){
		// run an HQL query on the BankAccount class
		// (remember that you must have annotated the BankAccount class
		// to define the mapping)
		session.beginTransaction();	
		List<BankAccount> allAccounts = session.createQuery("from BankAccount").list();
		session.getTransaction().commit();
		// iterate through the list and print details
		for (BankAccount ba: allAccounts){
			System.out.println("account no: " + ba.getAccountNumber() + 
					" name: " + ba.getAccountName() + " balance: " + ba.getBalance());
			//CHECK THE SUBCLASS 
			if (ba instanceof CurrentAccount){
				System.out.println(" overdraft: " + ((CurrentAccount)ba).getOverdraftLimit());
			}
			if (ba instanceof DepositAccount){
				System.out.println(" max. daily transactions: " + ((DepositAccount)ba).getMaxDailyTransactions());
			}
		}	
	}
	
	
	
	public void listAccountsAndTrans(){
		/*
		 * The BankAccount class has a variable called "transactions"
		 * of type Set<BankTransaction>. HQL treats this as the name of
		 * the association between the two classes. We can query by association 
		 * name, and HQL will figure out how to perform the relational join.
		 */
		session.beginTransaction();	
		
		Query qry = session.createQuery("from BankAccount bankAccount join bankAccount.transactions");
		List<Object[]> joinedData = qry.list();
		// this gets a little twisty... each List element is an Object array
		// we've joined 2 tables, so each Object array has 2 elements, 1 per table
		for (Object[] pair: joinedData){
			BankAccount ba = (BankAccount)pair[0];
			BankTransaction bt = (BankTransaction)pair[1];
			System.out.print("Account: " + ba.getAccountNumber() + " name: " + ba.getAccountName() +
					" balance: " + ba.getBalance());
			System.out.println(" trans: " + bt.getId() + " D/CR: " + bt.getDebitOrCredit() + " amount: " +
					bt.getAmount() + " date: " + bt.getDate());
		}
	}	
		
	
	
	public void withdrawCash() {
		// withdraw money from an account
		// first, prompt the user for a/c no. and amount
		int accNo = this.getInputAccountNumber("Account no. to withdraw from: ");
		double amt = this.getInputAmount();
		System.out.println("ATTEMPTING WITHDRAWAL OF " + amt + "FROM A/C NO. " + accNo);
		Transaction tran = null;
		try {
			// transaction starts...		
			tran = session.beginTransaction();
			//fetch the a/c from the database
			BankAccount ba = (BankAccount) session.get(BankAccount.class, new Integer(accNo));
			// perform withdrawal on POJO
			try {
				ba.withdraw(amt);
			} catch (BankAccount.InsufficientFundsException ife){
				System.out.println("NOT ENOUGH MONEY! " + ife.getMessage());
			}
			session.flush();
			// transaction ends...
			session.getTransaction().commit();
		} catch(Exception e){
			// rollback if possible
			try {
				tran.rollback();
			} catch(HibernateException he){
				he.printStackTrace();
			}
			e.printStackTrace();
		}
	}// end method withdrawCash
	
	
	// utility method: get input account number by prompting user
	public int getInputAccountNumber(String promptMsg){
		int accNo = 0;
		try {
			accNo = Integer.parseInt(JOptionPane.showInputDialog(promptMsg));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return accNo;
	}
	
	// utility method: get input name by prompting user
	public String getInputName(){
		String name = null;
		name = JOptionPane.showInputDialog("Please enter the new name");
		return name;
	}

	// utility method: get input money amount by prompting user
	public double getInputAmount(){
		double amount = 0;
		try {
			amount = Double.parseDouble(JOptionPane.showInputDialog("Please enter the amount to withdraw"));
		} catch(Exception e) {
			e.printStackTrace();
		}	
		return amount;
	}

	
	public static void main(String[] args) {
		// build an instance of this class and run its test methods
		BankAccountController bac = new BankAccountController();
		// initialise
		bac.init();
		// list all bank accounts
		bac.listAccounts();
		// withdraw from an account (if current a/c, should take overdraft into account)
		// then list all accounts with their transactions
		bac.withdrawCash();
		bac.listAccountsAndTrans();
		// finally, close the session
		bac.close();
	}

}
