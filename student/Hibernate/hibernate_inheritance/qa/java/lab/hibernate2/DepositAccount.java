package qa.java.lab.hibernate2;
import javax.persistence.*;

@Entity
// TO DO 
//
// set the discriminator value to "DA"
// 
// Note: this class does NOT REQUIRE an @Table annotation because we're using 
// the single table strategy... the table is therefore automatically the same as 
// for the superclass.
// 
public class DepositAccount extends BankAccount implements java.io.Serializable {

	@Column(name="max_number_trans")
	private int maxDailyTransactions;

	public int getMaxDailyTransactions() {
		return maxDailyTransactions;
	}

	public void setMaxDailyTransactions(int maxDailyTransactions) {
		this.maxDailyTransactions = maxDailyTransactions;
	}



}
